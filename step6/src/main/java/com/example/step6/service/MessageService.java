package com.example.step6.service;

import com.example.step6.domain.Message;

import java.util.List;

public interface MessageService {

 List<Message> findAll();
 List <Message> findByProfile(Long id);
 void addNewMessage (Long id,Message message);
}
