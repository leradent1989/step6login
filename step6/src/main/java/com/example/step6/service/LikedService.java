package com.example.step6.service;

import com.example.step6.dao.LikedDao;
import com.example.step6.domain.Profile;

import java.util.List;

  public interface LikedService {

    List<Profile> findAll();
    void addToList(Profile profile);

}
