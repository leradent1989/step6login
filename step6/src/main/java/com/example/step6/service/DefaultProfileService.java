package com.example.step6.service;
import com.example.step6.domain.Profile;
import com.example.step6.dao.JdbcProfileDao;

import java.util.List;

public class DefaultProfileService implements ProfileService {
    JdbcProfileDao jdbcProfileDao;


    public DefaultProfileService(JdbcProfileDao jdbcProfileDao) {
        this.jdbcProfileDao = jdbcProfileDao;
    }
@Override
    public List <Profile> findAll(){

        return  jdbcProfileDao.findAll();
    }
    @Override
    public List <Profile> findByIndex(Long index){
        return  jdbcProfileDao.findByIndex(index);
    }
    @Override
    public Profile findByLoginPass(String loginParam, String passwordParam){
        return jdbcProfileDao.findByLoginPass(loginParam,passwordParam);
    }
    public Profile  findById(Long index){
        return jdbcProfileDao.findById(index );
    }
}

