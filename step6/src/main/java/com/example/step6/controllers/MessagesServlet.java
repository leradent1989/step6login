package com.example.step6.controllers;

import com.example.step6.domain.Message;
import com.example.step6.domain.TemplateEngine;
import com.example.step6.service.DefaultMessageService;
import com.example.step6.service.DefaultProfileService;
import com.example.step6.service.MessageService;
import com.example.step6.service.ProfileService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

public class MessagesServlet extends HttpServlet {
    TemplateEngine templateEngine;
    MessageService defaultMessageService;
    ProfileService  defaultProfileService;

    public MessagesServlet(TemplateEngine templateEngine,DefaultMessageService defaultMessageService,DefaultProfileService defaultProfileService) {
        this.templateEngine = templateEngine;
        this.defaultMessageService = defaultMessageService;
        this.defaultProfileService = defaultProfileService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //  templateEngine.render("chat.html",resp);
          String userId ="";
        Cookie[] cookies = req.getCookies();
        Cookie cookie = Arrays.stream(cookies).filter(el -> el.getName().equals("userId")).findFirst().get();
        userId = cookie.getValue();



          String id=req.getParameter("id") ;

           if(id ==null){

               resp.sendRedirect("/messages?id=" + userId);
           }


                try{


                Long idLong = (long) Integer.parseInt(id);
                List<Message> messages = defaultMessageService.findByProfile(idLong);

                messages.forEach(el -> el.setAuthorProfile(defaultProfileService.findById(el.getAuthorId())));

                Map<String, Object> params = Map.of(
                        "messages", messages,
                        "messageForm", List.of(messages.get(0)),
                        "userprofile", List.of(defaultProfileService.findById(idLong))
                );

                templateEngine.render("chat.html", params, resp);}
                catch (NullPointerException e){}catch (IndexOutOfBoundsException err){}catch(NumberFormatException e){}catch (IllegalStateException e){System.out.println("");}
                    finally {
                    try{

                  req.getRequestDispatcher("/messages?id=" + req.getParameter("id")).forward(
                          req,resp);}catch (IllegalStateException e){System.out.println("");}catch (StackOverflowError e){}
                    finally {
                        try{resp.sendRedirect("/messages?id=" + req.getParameter("id"));}catch (IllegalStateException e){}
                    }
                }



    }
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String text = req.getParameter("new_message");

        String id =   req.getParameter("to");


        Long userId = (long) Integer.parseInt(id);
        List < Message > messages = defaultMessageService.findByProfile(userId);



      //  Random random = new Random();

        Cookie[] cookies = req.getCookies();
        Cookie cookie = Arrays.stream(cookies).filter(el -> el.getName().equals("userId")).findFirst().get();
        String idAuthor = cookie.getValue();
        Long authorId = (long) Integer.parseInt(idAuthor);
                //random.nextLong(0,3)+ 1L;

        Long idMessage = (long) (defaultMessageService.findAll().size() + 1);



        Message message = new Message(userId,authorId,text);
        System.out.println(message);


        defaultMessageService.addNewMessage(idMessage,new Message(userId,authorId,text));

        resp.sendRedirect("/messages?id=" + userId);



    }
}
