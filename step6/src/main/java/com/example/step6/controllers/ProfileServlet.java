package com.example.step6.controllers;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import com.example.step6.domain.Profile;
import com.example.step6.domain.TemplateEngine;
import com.example.step6.service.DefaultLikedService;
import com.example.step6.service.DefaultProfileService;
import com.example.step6.service.LikedService;
import com.example.step6.service.ProfileService;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;


public class ProfileServlet extends HttpServlet {
    public TemplateEngine templateEngine;
    //LikedDao likedDao ;

    public ProfileService defaultProfileService;

    LikedService defaultLikedService;

    public ProfileServlet(TemplateEngine templateEngine, DefaultLikedService defaultLikedService, DefaultProfileService defaultProfileService) {
        this.templateEngine = templateEngine;
        this.defaultLikedService =defaultLikedService;
        this.defaultProfileService = defaultProfileService;
    }

    Long count ;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");

        resp.getWriter().println("""
           
            <h2>Profiles:</h2>
            
        """);
        count=1L;

            Map<String, Object> params = Map.of(

                    "profiles",   defaultProfileService.findByIndex(count)
                    );

            templateEngine.render("profile2.html", params, resp);
        }


    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       String param1 = req.getParameter("param1");
        System.out.println(param1);
        String param2 = req.getParameter("param2");
        System.out.println(param2);
        if(param1 == null){
            param1 ="No";
        }
        if(param1.equals("YES")){
            Optional<Profile> repeatedProfile  = defaultLikedService.findAll().stream().filter(el -> el.getId().equals(defaultProfileService.findByIndex(count).get(0).getId())).findFirst();

         if(!repeatedProfile.isPresent()){
         defaultLikedService.addToList(defaultProfileService.findByIndex(count).get(0));}

        }
     if(count > defaultProfileService.findAll().size() -1){
         count=1L;
         String path = req.getContextPath() + "/liked";
         resp.sendRedirect(path);


     }
         Map<String, Object> params = Map.of(
                 "profiles", defaultProfileService.findByIndex(++count)
         );
         templateEngine.render("profile2.html", params, resp);

    }
}
