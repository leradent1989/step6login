package com.example.step6.controllers;

import com.example.step6.domain.TemplateEngine;
import com.example.step6.service.DefaultLikedService;
import com.example.step6.service.LikedService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

public class LikedServlet extends HttpServlet {
    public TemplateEngine templateEngine;

   LikedService defaultLikedService;
    public LikedServlet(TemplateEngine templateEngine,DefaultLikedService defaultLikedService) {

        this.templateEngine = templateEngine;
        this.defaultLikedService =defaultLikedService;
    }



    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        resp.getWriter().println("""
           
            <h2>Liked profiles</h2>
            
        """);


        Map<String, Object> params = Map.of(

                "profiles", defaultLikedService.findAll()
        );

        templateEngine.render("liked.html",params,resp);
    }



}
