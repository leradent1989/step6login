package com.example.step6;
import com.example.step6.controllers.*;
import com.example.step6.dao.JdbcLikedDao;
import com.example.step6.dao.JdbcMessageDao;
import com.example.step6.dao.JdbcProfileDao;
import com.example.step6.dao.LikedDao;
import com.example.step6.domain.TemplateEngine;
import com.example.step6.service.DefaultLikedService;
import com.example.step6.service.DefaultMessageService;
import com.example.step6.service.DefaultProfileService;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import javax.servlet.DispatcherType;
import java.util.ArrayList;
import java.util.EnumSet;


public class JettyRun  {
    public static void main(String[] args) throws Exception {
        Server server = new Server(8081);
        TemplateEngine templateEngine = new TemplateEngine();
        ServletContextHandler handler = new  ServletContextHandler();

        JdbcMessageDao jdbcMessageDao = new JdbcMessageDao();
        DefaultMessageService defaultMessageService = new DefaultMessageService(jdbcMessageDao);
        LikedDao likedDao = new JdbcLikedDao(new ArrayList<>());
        JdbcProfileDao jdbcProfileDao = new JdbcProfileDao();
        DefaultProfileService defaultProfileService = new DefaultProfileService(jdbcProfileDao);
        DefaultLikedService defaultLikedService = new DefaultLikedService(likedDao);
        ProfileServlet helloServlet = new ProfileServlet(templateEngine,defaultLikedService,defaultProfileService);
        LikedServlet likedServlet = new LikedServlet(templateEngine,defaultLikedService);
        MessagesServlet messagesServlet = new MessagesServlet(templateEngine,defaultMessageService,defaultProfileService);
        LoginServlet loginServlet = new LoginServlet(templateEngine,new DefaultProfileService(new JdbcProfileDao()));
        handler.addServlet( new ServletHolder(helloServlet),"/users");
        handler.addServlet( new ServletHolder(likedServlet),"/liked");
        handler.addServlet(new ServletHolder(messagesServlet),"/messages" );
        handler.addServlet( new ServletHolder(loginServlet),"/");
        handler.addServlet(CssBootstrapServlet.class, "/assets/css/bootstrap.min.css");

        handler.addFilter(new FilterHolder(new LoginFilter()),"/users",EnumSet.of(DispatcherType.INCLUDE,DispatcherType.REQUEST));
        handler.addFilter(new FilterHolder(new LoginFilter()),"/messages",EnumSet.of(DispatcherType.INCLUDE,DispatcherType.REQUEST));
       server.setHandler(handler);
       server.start();
       server.join();
    }
}
