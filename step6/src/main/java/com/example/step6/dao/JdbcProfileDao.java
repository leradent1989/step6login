package com.example.step6.dao;

import com.example.step6.domain.Profile;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JdbcProfileDao implements ProfileDao {


@Override
    public List <Profile> findAll(){
List <Profile> profiles=new ArrayList<>();

   try(Connection connection = getConnection()){
    PreparedStatement statement = connection.prepareStatement("SELECT * FROM users;");
    ResultSet rs = statement.executeQuery();

    while(rs.next()){
     Long profileId= rs.getLong(1);
       String name = rs.getString(2);
       String surname = rs.getString(3);
       String imgUrl = rs.getString(4);

       profiles.add(new  Profile(profileId,name,surname,imgUrl));
     //  profiles.forEach(el -> System.out.println(el.toString()));
    }

}catch (SQLException e){


}
        return profiles;
    }
    @Override
    public List <Profile>  findByIndex(Long index){
        List <Profile> profiles = new ArrayList<>();
        try(Connection connection = getConnection()){
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM users WHERE id = ?");
            statement.setLong(1,index);
            ResultSet rs = statement.executeQuery();
            if(rs.next()){

             Long id = rs.getLong(1);
               String name = rs.getString(2);
               String surname = rs.getString(3);
               String url = rs.getString(4);
               profiles.add( new Profile(id,name,surname,url));
            }
        }catch(SQLException e){}


        return profiles;
    }
    public Profile  findById(Long index){

        try(Connection connection = getConnection()){
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM users WHERE id = ?");
            statement.setLong(1,index);
            ResultSet rs = statement.executeQuery();
            if(rs.next()){

                Long id = rs.getLong(1);
                String name = rs.getString(2);
                String surname = rs.getString(3);
                String url = rs.getString(4);
              return   new Profile(id,name,surname,url);
            }
        }catch(SQLException e){}


        return null;
    }
@Override
    public  Profile findByLoginPass(String loginParam, String passwordParam) {

        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM users WHERE  login=? AND password=?");
            statement.setString(1, loginParam);
            statement.setString(2, passwordParam);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                Long id = rs.getLong(1);
              String name = rs.getString(2);
              String surname = rs.getString(3);
             String imgUrl = rs.getString(4);

                return new Profile(id,name,surname,imgUrl);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


}
