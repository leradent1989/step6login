package com.example.step6.dao;

import com.example.step6.domain.Message;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import  java.util.List;

public class JdbcMessageDao implements MessageDao {


    public  List <Message> findAll (){
       List <Message> messages = new ArrayList<>();
        try(Connection connection = getConnection()){
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM messages;");
            ResultSet rs = statement.executeQuery();
while(rs.next()){

    Long userId =  rs.getLong(2);
    Long authorId= rs.getLong(3);
    String text = rs.getString(4);

    messages.add(new Message(userId,authorId,text));


}

       }catch (SQLException e){



        }
       return messages;
    }
    public  List <Message> findByProfile (Long id) {
        List<Message> chat = new ArrayList<>();

        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM messages WHERE user_id = ?");
          statement.setLong(1,id);
            ResultSet res = statement.executeQuery();
            while (res.next()) {
                Long idDb = res.getLong(1);
                Long userId =  res.getLong(2);
                Long authorId= res.getLong(3);
                String text = res.getString(4);

                chat.add(new Message(userId,authorId,text));

            }


        } catch (SQLException e) {

        }

        // return    messageList.stream().filter(el -> el.getProfile().getId().equals(id)).collect(Collectors.toList());


        return chat;

    }

    public  void addNewMessage (Long id,Message message){

        try(Connection connection = getConnection() ){
     PreparedStatement statement = connection.prepareStatement("INSERT INTO messages (id, user_id, author_id,text) " +
             "VALUES (" + id + "," + message.getUserId() + ", " + message.getAuthorId() + ",'" + message.getText() + "')");
     int count = statement.executeUpdate();




        }catch (SQLException e){
            e.printStackTrace();
        }




    }


}
