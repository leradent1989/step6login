package com.example.step6.dao;

import com.example.step6.domain.Message;
import org.postgresql.ds.PGPoolingDataSource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface MessageDao {
  List<Message> findAll ();
  List <Message> findByProfile (Long id);
   void addNewMessage (Long id,Message message);
    default Connection getConnection()throws SQLException {
        PGPoolingDataSource source = new PGPoolingDataSource();

        source.setServerName("localhost:5432");
        source.setDatabaseName("tinderapp");
        source.setUser("postgres");
        source.setPassword("root");
        source.setMaxConnections(10);
        source.getConnection();
        return source.getConnection();


    }

}
